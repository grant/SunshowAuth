//
//  SXAppDelegate.m
//  尚秀授权
//
//  Created by sunshow-imac on 12-9-24.
//  Copyright (c) 2012年 sunshow-imac. All rights reserved.
//

#import "SXAppDelegate.h"

#import "SXViewController.h"

@implementation SXAppDelegate

- (void)dealloc
{
    [_window release];
    [_viewController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        self.viewController = [[[SXViewController alloc] initWithNibName:@"SXViewController_iPhone" bundle:nil] autorelease];
    } else {
        self.viewController = [[[SXViewController alloc] initWithNibName:@"SXViewController_iPad" bundle:nil] autorelease];
    }
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [self validation];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    UITextField *passwdField = [alertView textFieldAtIndex:0];
    if (passwdField && passwdField.text && passwdField.text.length>0)
    {
        if ([passwdField.text isEqualToString:@"12345qwer"]) {
            return;
        }
    }
    
    [self validation];
}

- (void)validation
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"用户验证" message:@"请输入密码:" delegate:self cancelButtonTitle:@"验证" otherButtonTitles:nil, nil];
    [alert setAlertViewStyle:UIAlertViewStyleSecureTextInput];
    [alert show];
    [alert release];
}

@end
