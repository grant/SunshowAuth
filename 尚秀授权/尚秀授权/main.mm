//
//  main.m
//  尚秀授权
//
//  Created by sunshow-imac on 12-9-24.
//  Copyright (c) 2012年 sunshow-imac. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SXAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SXAppDelegate class]));
    }
}
