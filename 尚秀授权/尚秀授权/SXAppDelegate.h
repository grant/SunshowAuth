//
//  SXAppDelegate.h
//  尚秀授权
//
//  Created by sunshow-imac on 12-9-24.
//  Copyright (c) 2012年 sunshow-imac. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SXViewController;

@interface SXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) SXViewController *viewController;

@end
