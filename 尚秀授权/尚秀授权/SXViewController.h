//
//  SXViewController.h
//  尚秀授权
//
//  Created by sunshow-imac on 12-9-24.
//  Copyright (c) 2012年 sunshow-imac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SXViewController : UIViewController<UIAlertViewDelegate>


@property (nonatomic, assign) IBOutlet UITextField *miniCodeField;
@property (nonatomic, assign) IBOutlet UITextField *padCodeField;
@property (nonatomic, assign) IBOutlet UITextView *finalCodeView;


- (IBAction)finishEdit:(id)sender;
- (IBAction)showCodeButtonClicked:(id)sender;
- (IBAction)photoLibBtnClicked:(id)sender;
@end
