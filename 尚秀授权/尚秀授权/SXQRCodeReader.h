//
//  SXQRCodeReader.h
//  尚秀授权
//
//  Created by Guangtao Li on 13-10-7.
//  Copyright (c) 2013年 sunshow-imac. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SXQRCodeReaderDelegate <NSObject>
@required
- (void)decodedWithMiniCode:(NSString *)miniCode padCode:(NSString *)padCode;
- (void)decodeFaild;
@end



@interface SXQRCodeReader : NSObject
- (id)initWithImage:(UIImage *)image delegate:(id<SXQRCodeReaderDelegate>) delegate;
@property (nonatomic, assign) id<SXQRCodeReaderDelegate> delegate;
@end
